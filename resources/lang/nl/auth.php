<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Deze combinatie van e-mailadres en wachtwoord is niet geldig.',
    'throttle' => 'Teveel mislukte login pogingen. Probeer het over :seconds seconden nogmaals.',
    'inactive' => 'Dit account is nog niet actief en kan daarom niet inloggen.',
    'username' => 'E-mailadres',
    'password' => 'Wachtwoord',
    'login'    => 'Inloggen',
    'logout'   => 'Uitloggen',
    'register' => 'Registreren',
    'reset'    => 'Wachtwoord vergeten?',
    'remember' => 'Gegevens onthouden',


];
