<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Daniël van der Linden',
            'email' => 'daniel@spaggel.nl',
            'password' => bcrypt('secret'),
        ]);
    }
}
